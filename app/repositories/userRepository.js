const { User } = require("../models");
const bcrypt = require('bcrypt');

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy({
      where: {
        id
      }
    });
  },

  find(id) {
    return User.findByPk({
      attributes:  {exclude: ['password']},
      where: {
        id,
      }    
    });
  },

  findAll() {
    return User.findAll({attributes:  {exclude: ['password']},});
  },

  getTotalUser() {
    return User.count();
  },

  // register
  async registerNewUser(createArgs) {
    console.log('ini repository')

    const user = await User.findOne({
      where: {
        email : createArgs.email
      }
    })

    // check if user not exist
    if(user){
      if (user.email === createArgs.email) {
        throw new Error(`user with email : ${user.email} already taken`)
      }
    }
    return User.create(createArgs);

  },

  async login(userArgs) {
    console.log(userArgs)
    const user = await User.findOne({
      where: {
        email: userArgs.email
      }
    });

    if(user){
      const validation = await bcrypt.compare(userArgs.password, user.password);
      if(validation){
        return User.findOne({
          attributes: {exclude: ['password']},
          where: {
            email: userArgs.email,
          }    
        });
      }else{
          throw new Error("Password Salah")
      }
    }else{
      throw new Error("User Tidak ditemukan")
    }

  }
};

// module.exports = {
//   create
//   register
//   fafwfwfw
// }
